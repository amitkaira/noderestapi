import { WealthMakerController } from '../../../app/controllers/WealthMakerController';
import { WealthMakerRoute } from './../WealthMakerRoutes';
import express = require('express');
import { LeadRoutes } from '../LeadRoutes';


var app = express();

class BaseRoutes{

    get routes (){
        // declare all the routes here
        app.use("/", new WealthMakerRoute().routes);
        app.use("/", new LeadRoutes().routes);
        return app;
    }
}
export = BaseRoutes;


