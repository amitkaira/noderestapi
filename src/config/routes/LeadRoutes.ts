import { LeadController } from './../../app/controllers/LeadController';
import express = require("express");
const chalk = require("chalk");
const log = console.log;
var router = express.Router();

export class LeadRoutes{

    private _LeadController : LeadController;

    constructor(){
        this._LeadController = new LeadController();
    }

    /**
     * generates all the routes for the lead collection
     * @readonly
     * @memberof LeadRoutes
     */
    get routes(){
        log(chalk.red('LEAD-ROUTES'));
        var nController = this._LeadController;
        router.get("/testLeadAPI",nController.testAPI);
        router.post("/createLead",nController.createLead);
        router.get("/allLeads",nController.getAllLeads);
        return router;
    }
}
Object.seal(LeadRoutes);