import express = require("express");
const chalk = require("chalk");
const log = console.log;
import { WealthMakerController } from '../../app/controllers/WealthMakerController';
var router = express.Router();

export class WealthMakerRoute{
    
    private _WMController : WealthMakerController;

    constructor(){
        this._WMController = new WealthMakerController();
    }
    
    get routes (){
        var nController = this._WMController;
        router.get("/welcome", nController.welcome);
        router.post("/create", nController.createEntry);
        router.get("/getAll", nController.getAllEntries);
        return router;
    }
}
Object.seal(WealthMakerRoute);

