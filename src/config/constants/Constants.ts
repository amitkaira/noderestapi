class Constants {
    
    static DB_CONNECTION_STRING: string  = "mongodb://localhost/WealthMakerDB"; 
}
Object.seal(Constants);
export = Constants;
