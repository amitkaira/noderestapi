import RepositoryBase = require('./base/RepositoryBase');
import LeadSchema = require('../dataAccess/schema/LeadSchema');
import ILeadDocument = require('./../models/LeadModel');

class LeadRepo extends RepositoryBase.RepositoryBase<ILeadDocument.ILeadDocument>{
    
        constructor () {
            super(LeadSchema);
        }
    }
    
Object.seal(LeadRepo);
export = LeadRepo;
