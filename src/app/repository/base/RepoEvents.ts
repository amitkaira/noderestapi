import mongoose = require('mongoose');


// all events which will read data from the database
export interface IRead<T>{
  retrieve: (callback:(error:any, result:any)=> void)=>void;
  findById:(id:string, callback:(error:any, result:T)=>void)=>void;   
}

// all events which will write data into the database
export interface IWrite<T>{
 create:(item:T, callback: (error:any, result:any)=>void)=>void;
 update:(_id:mongoose.Types.ObjectId,item:T, callback:(error:any, result:any)=>void)=>void;
 delete:(_id:string, callback:(error:any, result:any)=>void)=>void;
}


