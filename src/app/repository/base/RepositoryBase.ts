import { WealthMakerModel } from './../../models/WealthmakerModel';
import { IRead, IWrite } from './RepoEvents';
import mongoose = require("mongoose");

export class RepositoryBase<T extends mongoose.Document> implements IRead<T>, IWrite<T>{

    private _model: mongoose.Model<mongoose.Document>;

    constructor(schemaModel:mongoose.Model<mongoose.Document>){
        this._model = schemaModel;
    }


    retrieve(callback:(error:any, result:any)=> void){
        this._model.find({}, callback).limit(2);
    }

    
    findById(_id:string, callback:(error:any, result:T)=>void){
        this._model.findById( _id, callback);
    }

    
    create(item:T, callback: (error:any, result:any)=>void){
        this._model.create(item,callback);
    }


    update(_id:mongoose.Types.ObjectId,item:T, callback:(error:any, result:any)=>void){

    }

    delete(_id:string, callback:(error:any, result:any)=>void){

    }

}