import WealthMakerSchema = require('../dataAccess/schema/WealthMakerSchema');
import IWealthMakerDocument = require('../models/WealthmakerModel');
import RepositoryBase =require('./base/RepositoryBase');

 class WealthMakerRepo extends RepositoryBase.RepositoryBase<IWealthMakerDocument.IWealthMakerDocument>{

    constructor () {
        super(WealthMakerSchema);
    }
}

Object.seal(WealthMakerRepo);
export = WealthMakerRepo;