import WealthMakerRepo = require('./../repository/WealthMakerRepo');
import { WealthMakerModel,IWealthMakerDocument } from '../models/WealthmakerModel';
import express = require('express');
import { Response } from 'express';
const chalk = require('chalk');
const log = console.log;


export class WealthMakerController{
    
    private TAG:String = "WEALTH-MAKER-CONTROLLER";
    
    welcome(req: express.Request, res:express.Response):void{
        try{
            var data = req.query;
            log(chalk.green.underline.bold('Welcome')+"-"+chalk.green(''+JSON.stringify(data)));
            var response  = "<h2><span><strong>Welcome !&nbsp; API exceuting successfully</strong></span></h2>"+
            "<h4><span ><strong>All api are as follows....</strong></span></h4>"+
            "<p>1. <span>create</span> - POST</span> - <span >create log for wealthmaker</span></p>"+
            "<p>2. <span>getAll</span> -- GET</span> -- <span >get all logs from wealthmaker</span></p>"+
            "<p>3. <span>testLeadAPI</span> -- GET</span> -- <span >get all leads saved in database</span></p>"+
            "<p>&nbsp;</p>";
            //res.send({"status":"1", "message":"Welcome to WealthMaker API Services", "data":data});
            res.send(response);
        }catch(e){
            log(chalk.red('welcome'+chalk.blue.underline.bold(''+e)));
            res.send({"status":"0", "message":"Error Occured asdasdasdasd: "+e});
        }
    }
 

    /**
     * This function retrives all the records in the database collection
     * @param req 
     * @param res 
     */
    getAllEntries(req:express.Request, res:Response):void{
        try {
            var repo: WealthMakerRepo = new WealthMakerRepo();
            repo.retrieve((error, result) => {
                if(error) res.send({"error": "error"});
                else res.send({"status":"1", "message":"Request exceuted succesfully","data":result});
            }); 
        } catch (e) {
            log(chalk.red('getAllEntries'+chalk.blue.underline.bold(''+e)));
            res.send({"status":"1", "message":"Error Occured : "+e});
        }
    }

 /**
  * This function creates the new entry in the database 
  * @param {express.Request} req 
  * @param {express.Response} res 
  * @memberof WealthMakerController
  */
 createEntry(req: express.Request, res:express.Response):void{
        try{
            //get data from repository
            var data = req.body;
            log(chalk.green(chalk.blue.underline.bold('Params :')+JSON.stringify(data)));
            // typecast it to the specified model
            var model : IWealthMakerDocument = <IWealthMakerDocument>data;
           
            // call repository to update database
            var repo: WealthMakerRepo = new WealthMakerRepo();
            repo.create(model, (error, result)=>{
                if(error){
                    res.send({"status":"o", "message":"Error :"+JSON.stringify(error)});
                }else{
                    res.send({"status":"1", "message":"Entry created succesfully."});
                }
            });
            log(chalk.green.underline.bold('Welcome')+"-"+chalk.green(''+JSON.stringify(data)));
          }catch(e){
            log(chalk.red('CreateEntry'+chalk.blue.underline.bold(''+e)));
            res.send({"status":"1", "message":"Error Occured : "+e});
        }
    }
}
