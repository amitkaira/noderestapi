import mongoose = require('mongoose');

export interface IWealthMakerDocument extends mongoose.Document{
    globalIP:string,
    subnetIP:string;
    hostName:string;
    time:string;
    infoType:string;
    message:string;
}

export class WealthMakerModel  {
    
        private _wealthMakerDocument: IWealthMakerDocument;
    
        constructor(wealthMakerModel: IWealthMakerDocument){
            this._wealthMakerDocument = wealthMakerModel;
        }
    
        get globalIP (): string{
            return this._wealthMakerDocument.globalIP;
        }
      
        get subnetIP (): string {
            return this._wealthMakerDocument.subnetIP;
        }
        
        get hostName (): string {
            return this._wealthMakerDocument.hostName;
        }
    
        get infoType (): string {
            return this._wealthMakerDocument.infoType;
        }
      
        get time (): string {
            return this._wealthMakerDocument.time;
        }
    
        
        get message (): string {
            return this._wealthMakerDocument.message;
        }
    } 

    Object.seal(WealthMakerModel);
    