import mongoose = require('mongoose');

export interface ILeadDocument extends mongoose.Document{
    leadID:string,
    clientName:string;
    mobileNumber:string;
    email:string;
    time:Date; // System Generated
    creationDate:Date; // System Generated
    productCategory:string;
    productSubCategory:string;
    CSOName:string;
    CSOID :string;
    RMName:string;
    leadSource:string;
    campaignID:string;
}

// {
//     "leadID":"101",
//         "clientName":"";
//         "mobileNumber":"";
//         "email":"";
//         "time":""; // System Generated
//         "creationDate":""; // System Generated
//         "productCategory":"";
//         "productSubCategory":"";
//         "CSOName":"";
//         "CSOID" :"";
//         "RMName":"";
//         "leadSource":"";
//         "campaignID":"";
//     }

export class LeadModel {

    private _LeadDocument : ILeadDocument;

    constructor(leadDocument : ILeadDocument){
        this._LeadDocument = leadDocument;
    }
    
    get leadID (): string{
        return this._LeadDocument.leadID;
    }
    
    get clientName (): string{
        return this._LeadDocument.clientName;
    }
    
    get mobileNumber (): string{
        return this._LeadDocument.mobileNumber;
    }

    get email (): string{
        return this._LeadDocument.email;
    }

    get time (): string{
        return this._LeadDocument.time;
    }
    
    get leacreationDatedID (): string{
        return this._LeadDocument.creationDate;
    }
    
    get productCategory (): string{
        return this._LeadDocument.productCategory;
    }
    
    get productSubCategory (): string{
        return this._LeadDocument.productSubCategory;
    }
    
    get CSOName (): string{
        return this._LeadDocument.CSOName;
    }
    
    get CSOID (): string{
        return this._LeadDocument.CSOID;
    }
    
    get RMName (): string{
        return this._LeadDocument.RMName;
    }
    
    get leadSource (): string{
        return this._LeadDocument.leadSource;
    }

    get campaignID (): string{
        return this._LeadDocument.campaignID;
    }
}
Object.seal(LeadModel);