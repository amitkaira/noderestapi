import { IWealthMakerDocument } from './../../models/WealthmakerModel';
import WealthMakerModel= require ('./../../models/WealthmakerModel');

import DataAccess = require("../DataAccess");
var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;

class WealthMakerSchema {

    static get schema(){
        var WealthSchema = new mongoose.Schema({
            globalIP: {
                type:String,
                required:false
            },

            subnetIP: {
                type:String,
                required:false
            },
            hostName: {
                type:String,
                required:false
            },
            time: {
                type:String,
                required:true
            },
            infoType: {
                type:String,
                required:true
            },
            message: {
                type:String,
                required:true
            }
        });
        WealthSchema.set('toJSON', {
            virtuals: true,
            transform: (doc, ret, options) => {
                ret.id = ret._id.toString();
                delete ret._id;
                delete ret.__v;
            },
        });
        return WealthSchema;
    }
}
 var schema = mongooseConnection.model<IWealthMakerDocument>("WealthMaker", WealthMakerSchema.schema); 
 module.exports = schema;
 // export this to other classes