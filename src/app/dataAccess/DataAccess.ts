import { error } from 'util';
const chalk = require("chalk");
import Mongoose = require('mongoose');
import Constants = require('../../config/constants/Constants');

class DataAccess {
        static mongooseInstance: any;
        static mongooseConnection: Mongoose.Connection;
        
        constructor () {
            DataAccess.connect();
        }
        
        static connect (): Mongoose.Connection {
            if(this.mongooseInstance) return this.mongooseInstance;
            this.mongooseConnection  = Mongoose.connection;
            this.mongooseConnection.once("openUri", () => {
                console.log(chalk.yellow.underline.bold("Connecting to MongoDB Server"));
            });
           this.mongooseInstance = Mongoose.connect(Constants.DB_CONNECTION_STRING);
           return this.mongooseInstance;
        }
        
    }
    DataAccess.connect();
    export = DataAccess;