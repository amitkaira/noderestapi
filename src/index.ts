 import express = require('express');
 import Middlewares = require("./config/middlewares/MiddlewaresBase");
 


 // set all configrations here
 var app = express();
 var port = parseInt(process.env.PORT, 1000) || 5000;
 

 // set setting to the app
 app.set("port",port);
 app.use(Middlewares.configuration);

 app.listen(port, ()=>{
    console.log("WebService App is running at localhost:"+port);
 });