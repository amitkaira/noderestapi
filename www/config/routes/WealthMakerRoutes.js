"use strict";
const express = require("express");
const chalk = require("chalk");
const log = console.log;
const WealthMakerController_1 = require('../../app/controllers/WealthMakerController');
var router = express.Router();
class WealthMakerRoute {
    constructor() {
        this._WMController = new WealthMakerController_1.WealthMakerController();
    }
    get routes() {
        var nController = this._WMController;
        router.get("/welcome", nController.welcome);
        router.post("/create", nController.createEntry);
        router.get("/getAll", nController.getAllEntries);
        return router;
    }
}
exports.WealthMakerRoute = WealthMakerRoute;
Object.seal(WealthMakerRoute);
