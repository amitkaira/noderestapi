"use strict";
const LeadController_1 = require('./../../app/controllers/LeadController');
const express = require("express");
const chalk = require("chalk");
const log = console.log;
var router = express.Router();
class LeadRoutes {
    constructor() {
        this._LeadController = new LeadController_1.LeadController();
    }
    /**
     * generates all the routes for the lead collection
     * @readonly
     * @memberof LeadRoutes
     */
    get routes() {
        log(chalk.red('LEAD-ROUTES'));
        var nController = this._LeadController;
        router.get("/testLeadAPI", nController.testAPI);
        router.post("/createLead", nController.createLead);
        router.get("/allLeads", nController.getAllLeads);
        return router;
    }
}
exports.LeadRoutes = LeadRoutes;
Object.seal(LeadRoutes);
