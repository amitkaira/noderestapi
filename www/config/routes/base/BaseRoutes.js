"use strict";
const WealthMakerRoutes_1 = require('./../WealthMakerRoutes');
const express = require('express');
const LeadRoutes_1 = require('../LeadRoutes');
var app = express();
class BaseRoutes {
    get routes() {
        // declare all the routes here
        app.use("/", new WealthMakerRoutes_1.WealthMakerRoute().routes);
        app.use("/", new LeadRoutes_1.LeadRoutes().routes);
        return app;
    }
}
module.exports = BaseRoutes;
