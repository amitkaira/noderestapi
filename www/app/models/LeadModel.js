"use strict";
// {
//     "leadID":"101",
//         "clientName":"";
//         "mobileNumber":"";
//         "email":"";
//         "time":""; // System Generated
//         "creationDate":""; // System Generated
//         "productCategory":"";
//         "productSubCategory":"";
//         "CSOName":"";
//         "CSOID" :"";
//         "RMName":"";
//         "leadSource":"";
//         "campaignID":"";
//     }
class LeadModel {
    constructor(leadDocument) {
        this._LeadDocument = leadDocument;
    }
    get leadID() {
        return this._LeadDocument.leadID;
    }
    get clientName() {
        return this._LeadDocument.clientName;
    }
    get mobileNumber() {
        return this._LeadDocument.mobileNumber;
    }
    get email() {
        return this._LeadDocument.email;
    }
    get time() {
        return this._LeadDocument.time;
    }
    get leacreationDatedID() {
        return this._LeadDocument.creationDate;
    }
    get productCategory() {
        return this._LeadDocument.productCategory;
    }
    get productSubCategory() {
        return this._LeadDocument.productSubCategory;
    }
    get CSOName() {
        return this._LeadDocument.CSOName;
    }
    get CSOID() {
        return this._LeadDocument.CSOID;
    }
    get RMName() {
        return this._LeadDocument.RMName;
    }
    get leadSource() {
        return this._LeadDocument.leadSource;
    }
    get campaignID() {
        return this._LeadDocument.campaignID;
    }
}
exports.LeadModel = LeadModel;
Object.seal(LeadModel);
