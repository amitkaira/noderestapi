"use strict";
class WealthMakerModel {
    constructor(wealthMakerModel) {
        this._wealthMakerDocument = wealthMakerModel;
    }
    get globalIP() {
        return this._wealthMakerDocument.globalIP;
    }
    get subnetIP() {
        return this._wealthMakerDocument.subnetIP;
    }
    get hostName() {
        return this._wealthMakerDocument.hostName;
    }
    get infoType() {
        return this._wealthMakerDocument.infoType;
    }
    get time() {
        return this._wealthMakerDocument.time;
    }
    get message() {
        return this._wealthMakerDocument.message;
    }
}
exports.WealthMakerModel = WealthMakerModel;
Object.seal(WealthMakerModel);
