"use strict";
const DataAccess = require("../DataAccess");
var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;
class LeadSchema {
    static get schema() {
        var _leadSchema = new mongoose.Schema({
            leadID: {
                type: String,
                required: false
            },
            clientName: {
                type: String,
                required: false
            },
            mobileNumber: {
                type: String,
                required: true
            },
            email: {
                type: String,
                required: true
            },
            time: {
                type: String,
                required: false
            },
            creationDate: {
                type: String,
                required: false
            },
            productCategory: {
                type: String,
                required: false
            },
            productSubCategory: {
                type: String,
                required: false
            },
            CSOName: {
                type: String,
                required: false
            },
            CSOID: {
                type: String,
                required: false
            },
            RMName: {
                type: String,
                required: false
            },
            leadSource: {
                type: String,
                required: false
            },
            campaignID: {
                type: String,
                required: false
            }
        });
        _leadSchema.set('toJSON', {
            virtuals: true,
            transform: (doc, ret, options) => {
                ret.id = ret._id.toString();
                delete ret._id;
                delete ret.__v;
            },
        });
        return _leadSchema;
    }
}
var schema = mongooseConnection.model("Lead", LeadSchema.schema);
module.exports = schema;
