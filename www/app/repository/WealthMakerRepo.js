"use strict";
const WealthMakerSchema = require('../dataAccess/schema/WealthMakerSchema');
const RepositoryBase = require('./base/RepositoryBase');
class WealthMakerRepo extends RepositoryBase.RepositoryBase {
    constructor() {
        super(WealthMakerSchema);
    }
}
Object.seal(WealthMakerRepo);
module.exports = WealthMakerRepo;
