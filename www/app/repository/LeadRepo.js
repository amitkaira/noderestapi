"use strict";
const RepositoryBase = require('./base/RepositoryBase');
const LeadSchema = require('../dataAccess/schema/LeadSchema');
class LeadRepo extends RepositoryBase.RepositoryBase {
    constructor() {
        super(LeadSchema);
    }
}
Object.seal(LeadRepo);
module.exports = LeadRepo;
