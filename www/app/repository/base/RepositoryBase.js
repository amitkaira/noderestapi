"use strict";
class RepositoryBase {
    constructor(schemaModel) {
        this._model = schemaModel;
    }
    retrieve(callback) {
        this._model.find({}, callback).limit(2);
    }
    findById(_id, callback) {
        this._model.findById(_id, callback);
    }
    create(item, callback) {
        this._model.create(item, callback);
    }
    update(_id, item, callback) {
    }
    delete(_id, callback) {
    }
}
exports.RepositoryBase = RepositoryBase;
