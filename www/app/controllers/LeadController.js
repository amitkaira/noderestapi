"use strict";
const chalk = require('chalk');
const log = console.log;
const LeadRepo = require('./../repository/LeadRepo');
class LeadController {
    constructor() {
        this.TAG = "LEAD-CONTROLLER";
    }
    testAPI(req, res) {
        try {
            var data = req.query;
            log(chalk.green.underline.bold('Welcome') + "-" + chalk.green('' + JSON.stringify(data)));
            res.send({ "status": "1", "message": "Hey.. Lead API are working..", "data": data });
        }
        catch (error) {
            log(chalk.red(this.TAG + ': testAPI :' + chalk.blue.underline.bold('' + error)));
            res.send({ "status": "0", "message": "Error Occured : " + error });
        }
    }
    createLead(req, res) {
        log(chalk.red('CREATE-LEAD' + chalk.blue.underline.bold('')));
        try {
            //get data from repository
            var data = req.body;
            log(chalk.green(chalk.blue.underline.bold('Params :') + JSON.stringify(data)));
            var leadModel = data;
            var repo = new LeadRepo();
            repo.create(leadModel, (error, result) => {
                if (error) {
                    res.send({ "status": "o", "message": "Error :" + JSON.stringify(error) });
                }
                else {
                    res.send({ "status": "1", "message": "Entry created succesfully." });
                }
            });
            log(chalk.green.underline.bold('Welcome') + "-" + chalk.green('' + JSON.stringify(data)));
        }
        catch (e) {
            log(chalk.red('CreateEntry' + chalk.blue.underline.bold('' + e)));
            res.send({ "status": "1", "message": "Error Occured : " + e });
        }
    }
    getAllLeads(req, res) {
        try {
            var repo = new LeadRepo();
            repo.retrieve((error, result) => {
                if (error)
                    res.send({ "error": "error" });
                else
                    res.send({ "status": "1", "message": "Leads fetched succesfully", "data": result });
            });
        }
        catch (e) {
            log(chalk.red('getAllEntries' + chalk.blue.underline.bold('' + e)));
            res.send({ "status": "1", "message": "Error Occured : " + e });
        }
    }
}
exports.LeadController = LeadController;
