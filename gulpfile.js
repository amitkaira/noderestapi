// const gulp = require('gulp');
// const ts = require('gulp-TypeScript');
// const nodemon  = require('gulp-nodemon');
// var notify = require('gulp-notify');
// var livereload = require('gulp-livereload');

// const tsProject = ts.createProject('tsconfig.json');

// // this task will run when the watcher will detect any changes
// gulp.task('scripts',()=>{
//     const tsResult = tsProject.src().pipe(ts(tsProject));
//     livereload.listen();
//         // configure nodemon
//         nodemon({
//             script:'./libs/index.js',
//             ext:'js'
//         }).on('restart',()=>{
//             gulp.src("./libs/index.js")
//                 // .pipe(livereload())
//                 // .pipe(notify('Reloading Page, Please wait ....'))
//         });

//        // return tsResult.js.pipe(gulp.dest('./libs'));
// });


// // starts the watcher
// gulp.task('watch',['scripts'],()=>{
//        gulp.watch('**/*.ts',['scripts']);
 
// });


// // inits the default task
// gulp.task('default',['watch'],()=>{
   

// });
const gulp = require('gulp');
const ts = require('gulp-TypeScript');
const nodemon  = require('gulp-nodemon');


const tsProject = ts.createProject('tsconfig.json');

// this task will run when the watcher will detect any changes
gulp.task('scripts',()=>{
    const tsResult = tsProject.src().pipe(ts(tsProject));
    return tsResult.js.pipe(gulp.dest('./www'));
});


// starts the watcher
gulp.task('watch',['scripts'],()=>{
    gulp.watch('**/*.ts',['scripts']);
});


// inits the default task
gulp.task('default',['watch']);


